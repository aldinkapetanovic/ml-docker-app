import numpy as np
import joblib

# Load the saved model
loaded_model = joblib.load('iris_model.pkl')

# Create new data for prediction
new_data = np.array([[5.1, 3.5, 1.4, 0.2],  # Sample 1
                     [6.2, 2.9, 4.3, 1.3],  # Sample 2
                     [7.3, 3.3, 6.0, 2.5],  # Sample 3
                     [1.9, 1, 1, 10]])  # Sample 4

# Use the loaded model to make predictions
predictions = loaded_model.predict(new_data)

# Print or use the predictions as needed
print("Predictions:", predictions)
